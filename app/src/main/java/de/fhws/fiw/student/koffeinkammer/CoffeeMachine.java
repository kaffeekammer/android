package de.fhws.fiw.student.koffeinkammer;

/**
 * model class for coffee machines
 * Created by simon on 19.06.17.
 */

public class CoffeeMachine {
    private String address;
    private String name;

    public CoffeeMachine(String address, String name) {
        this.address = address;
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package de.fhws.fiw.student.koffeinkammer;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MachinesListActivity extends AppCompatActivity {
    private static final String TAG = "MachinesListActivity";
    private Context mContext;

    private CoffeeMachineListAdapter machines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maschines_list);
        mContext = this;

        // get bluetooth permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        // configure list view
        machines = new CoffeeMachineListAdapter(mContext);
        ListView machinesListView = (ListView) findViewById(R.id.machines_list_view);
        machinesListView.setAdapter(machines);
        machinesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CoffeeMachine mCoffeeMachine = (CoffeeMachine) machines.getItem(position);
                Intent mIntent = new Intent(mContext, MachineActivity.class);
                mIntent.putExtra("deviceAddress", mCoffeeMachine.getAddress());
                mIntent.putExtra("deviceName", mCoffeeMachine.getName());
                startActivity(mIntent);
            }
        });

        //machines.addItem(new CoffeeMachine("123", "CoffeeMaker"));

        // start bluetooth le scan
        scanForBluetoothDevices();
    }

    /**
     * TODO: start scan for bluetooth le devices
     */
    private void scanForBluetoothDevices() {
    }

    @Override
    protected void onDestroy() {
        // TODO: stop bluetooth le scan
        super.onDestroy();
    }
}

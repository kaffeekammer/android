package de.fhws.fiw.student.koffeinkammer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class MachineActivity extends AppCompatActivity {

    private final static String TAG = "MachineActivity";
    private Context mContext;
    private CoffeeMachine mCoffeeMachine;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothDevice;
    private CoffeeMachineScanCallback mCoffeeMachineScanCallback;

    private static final UUID UUID_COFFEE_SERVICE = UUID.fromString("c0ffee00-503e-4c75-ba94-3148f18d941e");
    private static final UUID UUID_LEVEL_CHARACTERISTIC = UUID.fromString("4c455645-4c00-1000-8000-00805f9b34fb");
    private static final UUID UUID_DRINK_CUP_CHARACTERISTIC = UUID.fromString("4452494e-4b00-1000-8000-00805f9b34fb");
    private static final UUID UUID_FILL_UP_CHARACTERISTIC = UUID.fromString("46494c4c-5550-1000-8000-00805f9b34fb");

    private final static String ACTION_DISCOVER_SERVICES = "discoverServices";
    private final static String ACTION_DRINK_CUP = "drinkCupOfCoffee";
    private final static String ACTION_FILL_UP = "fillUpCoffeeMachine";
    private final static int CUP_SIZE = 25;

    /**
     * called if button to drink a cup of coffee is pressed
     * TODO: drink a cup of coffee
     */
    private void drinkACupOfCoffee() {
        Log.i(TAG, "drinkACupOfCoffee: method called");
    }


    /**
     * called if button to fill up coffee machine is pressed
     * TODO: fill up the coffee machine
     */
    private void fillUpCoffeeMachine() {
        Log.i(TAG, "fillUpCoffeeMachine: method called");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine);

        mContext = this;

        Intent mIntent = getIntent();
        mCoffeeMachine = new CoffeeMachine(
                getIntent().getStringExtra("deviceAddress"),
                getIntent().getStringExtra("deviceName"));

        // bottom navigation
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // set some text views
        TextView tvName = (TextView) findViewById(R.id.device_name);
        TextView tvAddress = (TextView) findViewById(R.id.device_address);

        tvName.setText(mCoffeeMachine.getName());
        tvAddress.setText(mCoffeeMachine.getAddress());

        // get bluetooth device
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(mCoffeeMachine.getAddress());

        // scan for advertisements with fill level
        mCoffeeMachineScanCallback = new CoffeeMachineScanCallback();
        mBluetoothAdapter.getBluetoothLeScanner().startScan(mCoffeeMachineScanCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBluetoothAdapter.getBluetoothLeScanner().stopScan(mCoffeeMachineScanCallback);
    }

    /**
     * updates the coffee level bar
     *
     * @param level new coffee level (0 - 255)
     */
    private void updateCoffeeLevel(int level) {
        ProgressBar mProgressBar = (ProgressBar) findViewById(R.id.coffee_level);
        mProgressBar.setProgress(level);
    }

    /**
     * listener object for bottom navigation
     */
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_drink_coffee:
                    drinkACupOfCoffee();
                    return false;
                case R.id.navigation_fill_up:
                    fillUpCoffeeMachine();
                    return false;
            }
            return false;
        }

    };

    /**
     * callback for bluetooth le scan
     * updates the coffee level if advertising is received
     */
    private class CoffeeMachineScanCallback extends ScanCallback {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            if (!mCoffeeMachine.getAddress().equals(result.getDevice().getAddress())) {
                return;
            }

            int level = result.getScanRecord().getBytes()[9] & 0xFF;
            updateCoffeeLevel(level);
            Log.i(TAG, "Coffee level: " + level);
        }
    }
}

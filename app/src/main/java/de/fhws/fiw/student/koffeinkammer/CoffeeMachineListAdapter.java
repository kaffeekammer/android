package de.fhws.fiw.student.koffeinkammer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Base Adapter for list of coffee machines
 * Created by simon on 19.06.17.
 */

class CoffeeMachineListAdapter extends BaseAdapter {
    private final List<CoffeeMachine> machines = new ArrayList<>();
    private final Context mContext;

    public CoffeeMachineListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return machines.size();
    }

    @Override
    public Object getItem(int position) {
        return machines.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CoffeeMachine machine = (CoffeeMachine) getItem(position);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View machineView = convertView;
        if(null == convertView) {
            machineView = inflater.inflate(R.layout.maschine_list_entry, null, false);
        }

        TextView tvName = (TextView) machineView.findViewById(R.id.device_name);
        TextView tvAddress = (TextView) machineView.findViewById(R.id.device_address);

        if (null != machine.getName()) {
            tvName.setText(machine.getName());
        } else {
            tvName.setText(R.string.unknown_device_name);
        }
        tvAddress.setText(mContext.getString(R.string.device_address_value, machine.getAddress()));

        return machineView;
    }

    /**
     * add coffee machine to list
     *
     * @param item item to add
     */
    public void addItem(CoffeeMachine item) {
        machines.add(item);
    }

    /**
     * get coffee machine by address string
     *
     * @param address address as string
     * @return CoffeeMachine
     */
    public CoffeeMachine getItem(String address) {
        for (CoffeeMachine cm : machines) {
            if (cm.getAddress().equals(address)) {
                return cm;
            }
        }

        return null;
    }
}
